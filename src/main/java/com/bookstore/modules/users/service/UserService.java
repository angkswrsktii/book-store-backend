package com.bookstore.modules.users.service;

import com.bookstore.modules.users.entity.UserEntity;
import com.bookstore.modules.users.repository.UserRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class UserService {
    private UserRepository userRepository;

    public UserEntity createUsers(UserEntity users) {
        return userRepository.save(users);
    }
}
